import React, { Component } from "react";
import { AsyncStorage, View, Text } from "react-native";
import firebase from "react-native-firebase";
import type { RemoteMessage } from "react-native-firebase";

export default class App extends Component {
  async componentDidMount() {
    this.messageListener = firebase
      .messaging()
      .onMessage((message: RemoteMessage) => {
        // Process your message as required
      });
    this.checkPermission();
  }
  componentWillUnmount() {
    this.messageListener();
  }
  //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  //3
  async getToken() {
    // let fcmToken = await AsyncStorage.getItem("fcmToken", value);
    // if (!fcmToken) {
    fcmToken = await firebase.messaging().getToken();
    if (fcmToken) {
      // user has a device token
      alert(fcmToken);
      await AsyncStorage.setItem("fcmToken", fcmToken);
    }
    // }
  }

  //2
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      alert("permission rejected");
      console.log("permission rejected");
    }
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Text>Vai tomar no cu!</Text>
      </View>
    );
  }
}
